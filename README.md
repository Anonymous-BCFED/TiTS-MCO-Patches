# TiTS: MCO - Patches
Trials in Tainted Space is a Flash text game by OXO Industries.

TiTS: Modular Code Overhaul is a toolkit that makes modding legal and possible.

This repository houses the patchset for the current public version of TiTS.

## Legal
TiTS is a product of OXO Industries. MCO Team is not affiliated with OXO Industries in any way, shape, or form.

TiTS: MCO is not endorsed, condoned, nor supported by OXO Industries, and is not a product of OXO.

Because of licensing restrictions, **you may not distribute the output binary *.SWF files, nor the modified code.** Sorry, but
[that's not under our control](https://github.com/OXOIndustries/TiTS-Public/blob/b7f7ada521a6f38ce07cecd55ad9d690dc1be1f2/LICENSE.md).  
Please petition Fenoxo for a more permissive code license.

## What
|Directory|Meaning|
|-|-|
|`patchsets`|Code patches in the form of universal diffs.|
|`mods`| Same, but for the optional mods. (WIP)|
|`tools/cleandiffs.py`| Breaks up the unified diff and post-processes it for cleanliness and to break it up into smaller files.|
|`tools/unfuckEncoding.py`|TiTS has a mix of UTF with BOM (utf-8), UTF-8 WITHOUT BOM (utf-8-sig), and Windows Code Page 1252 (cp1252).  This standardizes all the code to `utf-8-sig` (UTF-8 with no BOM).|
|`before`|TiTS code prior to being modified.|
|`base`|Baseline MCO changes, like changing perks and adding imports.|
|`after`|The patched code.|

## Development

1. [ ] Clone the official TiTS repo to a dir called `before`.
1. [ ] Clone a copy of it called `base` (you can clone from `before`)
1. [ ] Clone a copy of it called `after` (you can clone from `before`)
1. [ ] Checkout the commit used in this repo to both TiTS repos.
1. [ ] Run `python tools/unfuckEncoding.py` to standardize encoding (or you will get a lot of BOM-related cruft in the generated patches).
1. [ ] Apply **all** patchsets to `after`. Order does not matter.
1. [ ] Merge upstream changes or screw around with the code.
1. [ ] Run `GENPATCH.bat` or `GENPATCH.sh`
