import re, yaml

# include "../includes/SSTDs/sneezingTits.as";
REG_INCLUDES = re.compile(r'^[\t ]+include "([^"]+)";')

includes = []

with open('classes/TiTS.as', 'r') as f:
    for line in f:
        m = REG_INCLUDES.match(line.rstrip())
        if m is not None:
            includes += [m.group(1)]
print(f'Writing {len(includes)} includes to data/includes.yml...')
with open('data/includes.yml', 'w') as f:
    yaml.safe_dump(includes, f, default_flow_style=False)
