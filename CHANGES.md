Add stuff to this as you add MRs.  We'll eventually merge this with the primary CHANGELOG.md during a major release.

### Anonymous-BCFED:
* **Added** SSTD toggle in options menu (accessible during gameplay). Disables all processing of the poorly-designed SSTDs, and should remove any that are present.
* **Added** some cheaty "shittyships" and "shittyship" parts.  You can buy the OP-as-fuck SuperSpearheadSS via Vahn.
* MCO Internals:
  * Switched to simpler patching system, and uses smaller "repacked" version of TiTS to reduce download time. This repo packages images and other large binary files using Git LFS, and has a flattened repository history.
