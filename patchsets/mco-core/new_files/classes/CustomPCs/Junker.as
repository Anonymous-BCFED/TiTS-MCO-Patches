﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 * Updated 09/09/20
 */

package classes.CustomPCs {
	import classes.Characters.PlayerCharacter;
	import classes.Creature;
	import classes.Engine.Utility.*;
	import classes.GLOBAL;
	import classes.GameData.PerkData;
	// Name: Junker
	// Physical Description:
	// Dog everything (head, brown fur, tail, ears, etc.), except:
	// medium-long blonde hair
	// bipedal hooved legs
	// 2x 2"x12" horsedicks
	// 4x 2" balls
	// Perkswise, idc, whatever makes sense for a starting character. Nothing OP needed.
	public class Junker extends BaseCustomPC {
		public function Junker() {
			super();
			AddName("junker");
		}

		override public function ApplyTo(pc:PlayerCharacter):String {
			var bonusTexts:String = "";
			var i:int = 0;
			// Old code begins at includes/creation_custom_PCs.as:1213:
			if(!pc.hasCock()) pc.createCock();
			pc.createCock();
			pc.shiftCock(0,GLOBAL.TYPE_EQUINE);
			pc.shiftCock(1,GLOBAL.TYPE_EQUINE);
			pc.cocks[0].cLengthRaw = 12;
			pc.cocks[1].cLengthRaw = 12;
			pc.balls = 4;
			pc.ballSizeRaw = 7;
			pc.armType = GLOBAL.TYPE_CANINE;
			pc.clearArmFlags();
			pc.addArmFlag(GLOBAL.FLAG_FURRED);
			pc.legType = GLOBAL.TYPE_EQUINE;
			pc.clearLegFlags();
			pc.addLegFlag(GLOBAL.FLAG_DIGITIGRADE);
			pc.addLegFlag(GLOBAL.FLAG_FURRED);
			pc.addLegFlag(GLOBAL.FLAG_HOOVES);
			pc.tailCount = 1;
			pc.tailType = GLOBAL.TYPE_CANINE;
			pc.clearTailFlags();
			pc.addTailFlag(GLOBAL.FLAG_LONG);
			pc.addTailFlag(GLOBAL.FLAG_FURRED);
			pc.earType = GLOBAL.TYPE_CANINE;
			pc.clearEarFlags();
			pc.addEarFlag(GLOBAL.FLAG_FURRED);
			pc.addEarFlag(GLOBAL.FLAG_TAPERED);
			pc.faceType = GLOBAL.TYPE_CANINE;
			pc.hairLength = 6;
			pc.hairColor = "blonde";
			pc.skinType = GLOBAL.SKIN_TYPE_FUR;
			// End old code.
			return bonusTexts;
		}
	}
}

