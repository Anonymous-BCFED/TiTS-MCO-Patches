import codecs
import os
import shutil
import sys
import yaml
import ftfy
import tqdm
import collections
import logging
import typing


logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.handlers=[]
logger.addHandler(handler)
logger.setLevel(logging.INFO)
#logging.getLogger('patch').setLevel(logging.WARNING)

log = logging.getLogger('mco.unfuck')

ENCODINGS_DETECTED = {}

def unfuckEncodingInDir(rootdir):
    for root, _, files in os.walk(rootdir):
        for filename in files:
            if not filename.endswith('.as'):
                continue
            filepath = os.path.abspath(os.path.join(root, filename))
            unfuckFile(filepath)


def detect_encoding(filename: str) -> str:
    '''
    Attempts to detect encoding of filename using chardet.
    '''
    import chardet
    toread = min(32, os.path.getsize(filename))
    raw = b''
    with open(filename, 'rb') as f:
        raw = f.read(toread)
    encoding = 'utf-8-sig'
    bom = False
    if raw.startswith(codecs.BOM_UTF8):
        bom=True
        encoding = 'utf-8-sig'
    else:
        result = chardet.detect(raw)
        encoding = result['encoding']
        if encoding in ('utf-8', 'ascii'):
            encoding = 'utf-8-sig'
        if encoding in ('cp1252', 'Windows-1252'):
            encoding = 'cp1252'
    return encoding

def getEncodingOf(filepath, verbose=False):
    guessed_encoding = detect_encoding(filepath)
    success = False
    indented = False
    tmpfile = filepath + '.utf8'
    encoding = ''
    for enctry in [guessed_encoding, 'utf-8-sig', 'utf-8', 'cp1252', 'Windows-1252']:
        try:
            if verbose:
                if not indented:
                    log.info('Unfucking %s (%s)...', filepath, enctry)
                else:
                    log.info('%s...?', enctry)
            if os.path.isfile(tmpfile):
                os.remove(tmpfile)
            with codecs.open(filepath, 'r', enctry) as inf:
                with codecs.open(tmpfile, 'w', encoding='utf-8-sig') as outf:
                    for line in ftfy.fix_file(inf, encoding=enctry, fix_entities=False, fix_latin_ligatures=False, fix_character_width=False, uncurl_quotes=False):
                        outf.write(line)
            success = True
            encoding = enctry
            break
        except UnicodeDecodeError:
            if verbose:
                if not indented:
                    log.INDENT += 1
                log.error('Couldn\'t decode using %s! Trying another...', enctry)
            indented = True
            continue
        finally:
            if os.path.isfile(tmpfile):
                os.remove(tmpfile)
    if verbose and indented:
        log.INDENT -= 1
    if not success:
        log.error('Couldn\'t decode %s.', filepath)
        sys.exit(1)

    return encoding

def unfuckFile(filepath, verbose=False):
    fpkey = os.path.abspath(filepath).replace('\\', '/')
    guessed_encoding = detect_encoding(filepath) if filepath not in ENCODINGS_DETECTED else ENCODINGS_DETECTED[filepath]
    success = False
    indented = False
    for enctry in [guessed_encoding, 'utf-8-sig', 'utf-8', 'cp1252', 'Windows-1252']:
        try:
            if verbose:
                if not indented:
                    log.info('Fixing encoding for %s (%s)...', filepath, enctry)
                else:
                    log.info('%s...?', enctry)
            if os.path.isfile(filepath+'.utf8'):
                os.remove(filepath+'.utf8')
            with codecs.open(filepath, 'r', enctry) as inf:
                with codecs.open(filepath + '.utf8', 'w', encoding='utf-8-sig') as outf:
                    for line in ftfy.fix_file(inf, encoding=enctry, fix_entities=False, fix_latin_ligatures=False, fix_character_width=False, uncurl_quotes=False):
                        outf.write(line)
            add_bom = False
            with open(filepath + '.utf8', 'rb') as f:
                add_bom = f.read(3) != codecs.BOM_UTF8
            if add_bom:
                with open(filepath + '.utf8b', 'wb') as outf:
                    outf.write(codecs.BOM_UTF8)
                    with open(filepath + '.utf8', 'rb') as inf:
                        outf.write(inf.read())
                if os.path.isfile(filepath+'.utf8'):
                    os.remove(filepath+'.utf8')
                shutil.move(filepath + '.utf8b', filepath + '.utf8')
            if os.path.isfile(filepath):
                os.remove(filepath)
            shutil.move(filepath + '.utf8', filepath)
            success = True
            ENCODINGS_DETECTED[filepath] = enctry
            break
        except UnicodeDecodeError:
            if verbose:
                if not indented:
                    log.INDENT += 1
                log.error('Couldn\'t decode using %s! Trying another...', enctry)
            indented = True
            continue
    if verbose and indented:
        log.INDENT -= 1
    if not success:
        log.error('Couldn\'t decode %s.', filepath)
        sys.exit(1)

    # Standardize line endings
    contents = b''
    with open(filepath, 'rb') as f:
        contents = f.read()
    with open(filepath, 'wb') as f:
        f.write(contents.replace(b'\r\n', b'\n'))

def main():
    global ENCODINGS_DETECTED
    import argparse
    argp = argparse.ArgumentParser('unfuck.py', description='Fixes file encoding and line-endings.')
    argp.add_argument('filenames', type=str, nargs='+', help='Files or directories to unfuck.')
    argp.add_argument('--gen-report', type=str, help='Create a YAML file listing the properties of every located file.')
    argp.add_argument('--use-report', type=str, default=None, help='Use a YAML file listing the properties of every located file.')
    argp.add_argument('--report-starts-at', type=str, default='.', help='Relpath start point.')

    args = argp.parse_args()

    if args.use_report is not None:
        args.report_starts_at = os.path.abspath(args.report_starts_at)
        with open(args.use_report) as f:
            ENCODINGS_DETECTED = {os.path.abspath(os.path.join(args.report_starts_at, k)):v for k, v in yaml.safe_load(f).items()}

    todoList=[]
    print('Scanning for files...')
    for filename in args.filenames:
        if os.path.isdir(filename):
            print(filename + ' (dir)')
            n=0
            #unfuckEncodingInDir(filename)
            for root, _, files in os.walk(filename):
                for filepath in files:
                    if not filepath.endswith('.as'):
                        continue
                    todoList += [os.path.abspath(os.path.join(root, filepath))]
                    n += 1
            print(f'  {n} files')

        elif os.path.isfile(filename):
            print(filename + ' (file)')
            #unfuckFile(filename)
            todoList += [os.path.abspath(filename)]

        else:
            print('Could not find '+filename)

    print(f'{len(todoList)} files total')

    for entry in tqdm.tqdm(todoList, unit='f', desc=f'Fixing encoding in {len(todoList)} files...', ascii=True):
        unfuckFile(entry)

    if args.gen_report:
        args.report_starts_at = os.path.abspath(args.report_starts_at)
        data = {os.path.relpath(k, start=args.report_starts_at).replace('\\', '/'):v for k,v in ENCODINGS_DETECTED.items()}
        with open(args.gen_report, 'w') as f:
            yaml.safe_dump(data, f, default_flow_style=False)

if __name__ == '__main__':
    main()
