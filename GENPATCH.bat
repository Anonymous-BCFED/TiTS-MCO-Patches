::python tools/unfuck.py --gen-report="encoding.yml" --report-starts-at=before before
::python tools/unfuck.py --gen-report="base-encoding.yml" --report-starts-at=base base
::python tools/unfuck.py after
diff -uBEZNr -X diffignore.txt before/ base/ > patchsets/mco-core/mco-baseline.diff
diff -uBEZNr -X diffignore.txt base/ after/ > newpatch.diff
python tools/cleandiffs.py
