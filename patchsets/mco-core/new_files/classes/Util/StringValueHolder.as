﻿package classes.Util 
{
	/**
	 * Used to pass and hold a value for SimpleEvents.
	 * @author ...
	 */
	public class StringValueHolder
	{
		public var value:String;
		public function StringValueHolder(val:String) 
		{
			value = val;
		}
		
	}

}