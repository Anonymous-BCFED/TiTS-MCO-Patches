﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 * Updated 09/09/20
 */

package classes.CustomPCs {
	import classes.Characters.PlayerCharacter;
	import classes.Creature;
	import classes.Engine.Utility.*;
	import classes.GLOBAL;
	import classes.GameData.PerkData;
	public class Archie extends BaseCustomPC {
		public function Archie() {
			super();
			AddName("archie");
		}

		override public function ApplyTo(pc:PlayerCharacter):String {
			var bonusTexts:String = "";
			var i:int = 0;
			// Old code begins at includes/creation_custom_PCs.as:120:
			pc.breastRows[0].breastRatingRaw = 4;
			if(!pc.hasCock()) pc.createCock();
			pc.shiftCock(0,GLOBAL.TYPE_CANINE);
			if(pc.cocks[0].cLength() < 10) pc.cocks[0].cLengthRaw = 10;
			pc.hairLength = 3;
			pc.hairColor = "blue";
			if(pc.femininity < 80) pc.femininity = 80;
			if(pc.thickness < 70) pc.thickness = 70;
			if(pc.tone > 60) pc.tone = 60;
			if(pc.buttRatingRaw < 6) pc.buttRatingRaw = 6;
			if(pc.hipRatingRaw < 6) pc.hipRatingRaw = 6;
			// End old code.
			return bonusTexts;
		}
	}
}

