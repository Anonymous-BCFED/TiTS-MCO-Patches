﻿package classes.CustomPCs
{
	import classes.Characters.PlayerCharacter;
	import classes.Util.BoolValueHolder;
	/**
	 * A basic framework for modular custom PCs.
	 * @author Anonymous-BCFED
	 */
	public class BaseCustomPC
	{
		public var Names:Array = new Array();
		public function BaseCustomPC()
		{
		}

		public function Attach():void {
			//CustomPCRegistry.OnCustomPCCheck.Attach(this,_CustomPCCheck);
		}

		public function ApplyTo(pc:PlayerCharacter):String {
			return "";
		}

		protected function AddName(name:String):void {
			Names.push(name.toLowerCase());
		}

		public function matches(name:String):Boolean {
			if (Names.indexOf(name.toLowerCase()) != -1) {
				return true;
			}
			return false;
		}
		
		/**
		 * Whether to show the standard "Reality seems to warp" notice.
		 * @return yes/no
		 */
		public function getShowVanillaOutput(pc:PlayerCharacter): Boolean {
			return true;
		}
	}

}
